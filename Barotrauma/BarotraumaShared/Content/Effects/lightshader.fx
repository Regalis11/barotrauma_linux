Texture2D xLightTexture;
sampler LightSampler = sampler_state
{
    Texture = <xLightTexture>;
};

float cutoff;
float contrast;

float4 mainPostProcess(float4 position : SV_Position, float4 color : COLOR0, float2 texCoord : TEXCOORD0) : COLOR0
{	
    float4 lightClr = tex2D(LightSampler, texCoord);

    float brightness = clamp(dot(lightClr.rgb, float3(0.2126, 0.7152, 0.0722)), 0.0, 1.0);

    float4 outClr = lightClr;
    outClr.a = (brightness - cutoff) * contrast;
	
    return outClr;
}

technique LightShader
{
    pass Pass1
    {
        PixelShader = compile ps_4_0_level_9_1 mainPostProcess();
    }
}
