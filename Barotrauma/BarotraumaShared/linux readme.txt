To make the installer executable open a terminal in same folder as the script and write either
"chmod +740 Install.sh" or "chmod +740 ArchInstall.sh"(Without the quotation marks) and then write "./Install.sh" or "./ArchInstall.sh".

This will install the dependencies needed to run the game.
To then run the game you need to open a terminal in the same folder as the game and write "chmod +740 Launch_Barotrauma.sh" and then "./Launch_Barotrauma.sh".
